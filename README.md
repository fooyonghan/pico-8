Pico-8 projects I created.

Currently only contains a game called Party God, which I made for a friend. A tiny rhytm game that contains
gods, frogs and a whole lot of partying. All of the sprite art, animations, music and game mechanics
were made by me in a few days.

Party God is playable here: https://fooyonghan.itch.io/party-god